# bookmark-api

## What is it?

A simple CRUD API to post, retrieve and delete bookmarks

## Where's the demo?

https://mukundbhudia.gitlab.io/bookmark-api/swagger/
