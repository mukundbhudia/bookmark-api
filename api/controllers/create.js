'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/
var bookmark = require('../lib/bookmark');

module.exports = {
  create: create
};

function create(req, res) {
  const postedUrl = req.swagger.params.url.value;

  bookmark.generateHashFromURL(postedUrl).then(function(hashedUrl) {
    bookmark.storeBookmark(postedUrl);
    res.json(201, hashedUrl);
  }).catch(function() {
    res.json(400, null);
  })
  
}
