'use strict';

const url = require('url');
const crypto = require('crypto');

let bookmarks = new Map();

const generateHashFromURL = (postedUrl) => {
    return new Promise((resolve, reject) => {
        if (url.parse(postedUrl).hostname !== null) {
            const hashedUrl = crypto.createHash('sha224').update(postedUrl).digest('hex');
            resolve(hashedUrl);
        } else {
            reject(null);
        }
    });
};

const storeBookmark = (bookMark) => {
    generateHashFromURL(bookMark).then((hashedUrl) => {
        bookmarks.set(hashedUrl, bookMark);
    }).catch(() => {
        throw new Error("Bookmark not saved, not a URL");
    });
};

const getBookMark = (bookmarkId) => {
    return bookmarks.get(bookmarkId);
};

module.exports = {
    // bookmarks: bookmarks,
    storeBookmark: storeBookmark,
    getBookMark: getBookMark,
    generateHashFromURL: generateHashFromURL
};