var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function() {
  describe('create', function() {
    describe('POST /create', function() {

      const urlToTest = {
        url: 'https://www.bbc.co.uk/',
        hash: 'b9e597ff4b3516bc6b5141990dde3ce750f4a05a3c3bbd3ccc158a14'
      };

      const badUrlToTest = {
        url: 'some rubbish here',
        hash: null
      };

      it('should return a bookmark ID', function(done) {
        request(server)
          .post('/v1/create')
          .type('text/plain')
          .send(urlToTest.url)
          .set('Accept', 'text/plain')
          .expect('Content-Type', /json/)
          .expect(201)
          .end(function(err, res) {
            should.not.exist(err);
            res.body.should.eql(urlToTest.hash);
            done();
          });
      });

    it('should return a 400 error for a bad URL', function(done) {
        request(server)
          .post('/v1/create')
          .type('text/plain')
          .send(badUrlToTest.url)
          .set('Accept', 'text/plain')
          .expect('Content-Type', /json/)
          .expect(400)
          .end(function(err, res) {
            // console.log(res)
            should.not.exist(err);
            should.not.exist(res.body);
            done();
          });
      });
    });
  });
});
